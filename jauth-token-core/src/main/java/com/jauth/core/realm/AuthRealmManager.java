package com.jauth.core.realm;

import cn.hutool.core.util.StrUtil;
import com.jauth.core.AuthConfig;
import com.jauth.core.session.LocalSessionStorage;
import com.jauth.core.session.SessionStorage;
import com.jauth.core.token.DefaultTokenFactory;
import com.jauth.core.token.TokenFactory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录认证管理器
 * @author zengjintao
 * @create_at 2021年11月25日 0025 16:36
 * @since version 1.0.1
 */
public class AuthRealmManager {

    private final Map<String, LoginAuthRealm> loginAuthRealmMap = new HashMap<>();

    private final SessionStorage sessionStorage;

    private TokenFactory tokenFactory;

    private AuthConfig authConfig;

    public AuthRealmManager addLoginAuthRealm(LoginAuthRealm loginAuthRealm) {
        String loginType = loginAuthRealm.getLoginType();
        if (StrUtil.isNotBlank(loginType)) {
            loginAuthRealmMap.put(loginType, loginAuthRealm);
        } else {
            loginAuthRealmMap.put(loginAuthRealm.getClass().getSimpleName(), loginAuthRealm);
        }
        return this;
    }

    public AuthRealmManager(SessionStorage sessionStorage, TokenFactory tokenFactory, AuthConfig authConfig) {
        this.sessionStorage = sessionStorage;
        this.tokenFactory = tokenFactory;
        this.authConfig = authConfig;
    }

    public TokenFactory getTokenFactory() {
        return tokenFactory;
    }

    public AuthRealmManager() {
        this.sessionStorage = new LocalSessionStorage();
        this.tokenFactory = new DefaultTokenFactory();
        this.authConfig = new AuthConfig();
    }

    public AuthConfig getAuthConfig() {
        return authConfig;
    }

    public SessionStorage getSessionStorage() {
        return sessionStorage;
    }

    public List<LoginAuthRealm> getLoginAuthRealmList() {
        return (List<LoginAuthRealm>) loginAuthRealmMap.values();
    }

    public LoginAuthRealm getByLoginType(String loginType) {
        return loginAuthRealmMap.get(loginType);
    }
}
