package com.jauth.core;

/**
 * @author zengjintao
 * @create_at 2021年12月2日 0002 10:46
 * @since version 1.6.7
 */
public interface JAuthTimeConstants {

    long ONE_WEEK_MILLIS = 7 * 24 * 60 * 60 * 1000;

    long ONE_HOUR_MILLIS = 60 * 60 * 1000;
}
