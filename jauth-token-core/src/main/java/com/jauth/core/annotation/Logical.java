package com.jauth.core.annotation;

/**
 * @author zengjintao
 * @create_at 2021年11月26日 0026 17:10
 * @since version 1.0.1
 */
public enum Logical {

    AND,

    OR;
}
