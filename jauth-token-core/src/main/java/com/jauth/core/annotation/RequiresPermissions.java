package com.jauth.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 权限注解
 * @author zengjintao
 * @create_at 2021年11月26日 0026 17:07
 * @since version 1.0.1
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequiresPermissions {

    /**
     * 权限标识
     * @return
     */
    String[] value();

    /**
     * 并且、或者
     * @return
     */
    Logical logical() default Logical.AND;

    /**
     * 默认登录类型admin
     * @return
     */
    String loginType() default "admin";
}
